# SumoLogHandler

A custom LogHandler for [apple/swift-log](https://github.com/apple/swift-log) that sends messages to [SumoLogic](https://www.sumologic.com). 
It does not print to the console so you can use whatever LogHandler you want for console logs. I recommend [EmojiLogHandler](https://gitlab.com/onetapaway-opensource/swift/emojiloghandler) 🤓

## Getting Started

### Using a `package.swift` file

add this to your `dependencies` list
```swift
.package(url: "https://gitlab.com/onetapaway-opensource/swift/sumologhandler", from: "3.0.1")
```

and of course add `"SumoLogHandler"` to your list target dependencies
```swift
.target(name: "SweetProjectName", dependencies: ["SumoLogHandler"]),
```

### Using Xcode

File > Swift Packages > Add Package Dependency...

Paste the url for this project when prompted
```
https://gitlab.com/onetapaway-opensource/swift/sumologhandler
```


## Setup

In your App, bootstrap an instance of `SumoLogHandler`.

```swift
import SwiftUI
import Logging
import SumoLogHandler

public var logger: Logger = Logger(label: "com.mysweetapp")

@main
struct MySweetApp: App {
  
  init() {
    bootstrapLogger()
  }

  var body: some Scene {
    ...
  }

  private func bootstrapLogger() {

    let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String ?? "sweet-project-name"
    
    if let sumoUrlString = ProcessInfo.processInfo.environment["SUMO_URL"], let sumoUrl = URL(string: "https://\(sumoUrlString)") {
      LoggingSystem.bootstrap { label in
        MultiplexLogHandler([
          SumoLogHandler(
            label: label,
            sumoUrl: sumoUrl,
            sourceName: appName
          ),
          EmojiLogHandler(label: label)
        ])
      }
    } else {
      LoggingSystem.bootstrap { label in
        EmojiLogHandler(label: label)
      }
    }
    
    // now that we've registered log handlers, we can set logLevel and other metadata. 
    // logger.logLevel just forwards to the handlers so we need to do this after registering our handlers
#if DEBUG
    logger.logLevel = .trace
#else
    logger.logLevel = .info
#endif
    
    // any metadata you add to the logger will be sent along to Sumo
    if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
      logger[metadataKey: "appVersion"] = .string(appVersion)
    }
    if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
      logger[metadataKey: "buildNumber"] = .string(buildNumber)
    }

    // If you pass metadata to a single log message, it will be passed to sumo for that message only
    logger.info("this is the only message with foo:bar metadata", metadata: ["foo": "bar"])
    
  }
}

```

## Usage

Because SumoLogHandler uses [apple/swift-log](https://github.com/apple/swift-log), all you have to do is call the globally accessible logger that you set up in the previous step. (or just use the emoji functions set up by EmojiLogger)

```swift
logger.trace("trace message")
logger.debug("debug message")
logger.info("info message")
logger.notice("notice message")
logger.warning("warning message")
logger.error("error message")
logger.critical("critical message")
```

You can then query SumoLogic with things like
```
_sourceCategory="prod/mobile"
_sourceHost="ios"
_sourceName="sweet-project-name"
```

The log messages will look something like this
```
{
  "lineNumber":"84",
  "appVersion":"2.0",
  "function":"bootstrapLogger()",
  "fileName":"MySweetApp",
  "timestamp":"2023-11-22T10:32:35.315-0600",
  "foo":"bar",
  "message":"this is the only message with foo:bar metadata",
  "buildNumber":"1",
  "systemVersion":"17.0.1",
  "systemName":"iOS",
  "machine":"arm64",
  "logLevel":"info"
}
```


## SumoLogic Setup

You'll need to set up a new collector in sumo. Here's more information about that https://help.sumologic.com/Manage/Collection

I named mine `Mobile` and set the Source Category to `dev/mobile`. This is just the default that Sumo uses, but `SumoLogHandler` will overwrite it with whatever you set for `sumoCategory` which is defaulted to `prod/mobile`. This is customizable at any time so you can adjust to `dev/mobile`, `preprod/mobile`, or whatever you name your environments.
