//
//  SumoLogHandler.swift
//

import Logging
import Foundation
import UIKit

public class SumoLogHandler: LogHandler {
  
  public var metadata = Logger.Metadata()
  public var logLevel: Logger.Level = .debug
  public let label: String
  public let dateFormatter: DateFormatter
  
  private var urlSession = URLSession(configuration: URLSessionConfiguration.default)
  private let machine: String
  private let systemName: String
  private let systemVersion: String
  private var additionalLogInformation = [String: String]()

  private var logRotator = LogRotator()
  private var logUploader: LogUploader
  
  public init(
    label: String, // swift-log requires this
    sumoUrl: URL,
    sourceName: String,
    sourceHost: String = "ios",
    sourceCategory: String = "prod/mobile",
    dateFormatString: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
    enableConsolePrinting: Bool = false
  ) {
    consolePrintEnabled = enableConsolePrinting
    
    self.label = label
    self.logUploader = LogUploader(
      sumoUrl: sumoUrl,
      sourceName: sourceName,
      sourceHost: sourceHost,
      sourceCategory: sourceCategory
    )

    let formatter = DateFormatter()
    formatter.dateFormat = dateFormatString
    self.dateFormatter = formatter
    
    var systemInfo = utsname()
    uname(&systemInfo)
    self.machine = withUnsafeBytes(of: &systemInfo.machine) { rawPtr -> String in
      let ptr = rawPtr.baseAddress!.assumingMemoryBound(to: CChar.self)
      return String(cString: ptr)
    }
    self.systemName = UIDevice.current.systemName
    self.systemVersion = UIDevice.current.systemVersion
    
    let context = ["user": "\(label).sumoLogHandler"]
    let timer = Timer(timeInterval: 30.0, target: self, selector: #selector(uploadOldLogs), userInfo: context, repeats: true)
    RunLoop.current.add(timer, forMode: .common)
    
    NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
    printToConsole("sumoLogHandler.init finished")
  }
  
  @objc func uploadOldLogs(timer: Timer) {
    Task(priority: .background) {
      if await logUploader.isUploading {
        printToConsole("logUploader is busy. Not uploading old files right now")
        return // skip this round. We only want to upload old logs when nothing else is going on
      }
      printToConsole("checking for old file to clean up")
      if let fileToUpload = await logRotator.oldFileToCleanUp() {
        printToConsole("attempting to upload old log file \(fileToUpload.lastPathComponent)")
        await logUploader.upload(fileToUpload)
      }
    }
  }
  
  @objc func appMovedToBackground() {
    printToConsole("UIApplication.willResignActiveNotification")
    Task {
      await process(level: .info, message: "UIApplication.willResignActiveNotification", metadata: nil, file: #fileID, function: #function, line: #line, date: Date(), forceUpload: true)
    }
  }
  
  @objc func appMovedToForeground() {
    log(level: .info, message: "UIApplication.didBecomeActiveNotification", metadata: nil, file: #fileID, function: #function, line: #line)
    printToConsole("UIApplication.didBecomeActiveNotification")
  }
  
  public func log(level: Logger.Level, message: Logger.Message, metadata: Logger.Metadata?, file: String, function: String, line: UInt) {
    let date = Date() // grab this before dispatching to a different thread to ensure the most accurate date
    Task(priority: .background) {
      await self.process(level: level, message: message, metadata: metadata, file: file, function: function, line: line, date: date, forceUpload: level >= .error)
    }
  }
  
  /// converts  /some/dir/FileName.swift to FileName
  private func simplifiedFileName(_ file: String) -> String {
    guard let fileName = file.split(separator: "/").last else { return file }
    guard let withoutExtenstion = "\(fileName)".split(separator: ".").first else { return "\(fileName)" }
    return "\(withoutExtenstion)"
  }

  private func process(level: Logger.Level, message: Logger.Message, metadata: Logger.Metadata?, file: String, function: String, line: UInt, date: Date, forceUpload: Bool) async {
    
    var fileName = file
    if let fileWithoutExtension = file.components(separatedBy: "/").last?.components(separatedBy: ".").first {
      fileName = fileWithoutExtension
    }
    
    var data = [
      "message": "\(message)",
      "timestamp": dateFormatter.string(from: date),
      "logLevel": level.rawValue,
      "fileName": fileName,
      "lineNumber": "\(line)",
      "function": function,
      "machine": self.machine,
      "systemName": self.systemName,
      "systemVersion": self.systemVersion
    ]
    
    // add any metadata that we have
    if self.metadata.count > 0 {
      self.metadata.forEach { (key: String, metadataValue: Logger.MetadataValue) in
        let mappedValue = metadataToMap(key: key, metadataValue)
        data.merge(mappedValue, uniquingKeysWith: { $1 }) // Merge additional keys, keep the latest version
      }
    }
    // add any metadata that was passed in the log message. We do this last to ensure that it overwrites any conflicting keys
    if let logMetadata = metadata, logMetadata.count > 0 {
      logMetadata.forEach { (key: String, metadataValue: Logger.MetadataValue) in
        let mappedValue = metadataToMap(key: key, metadataValue)
        data.merge(mappedValue, uniquingKeysWith: { $1 }) // Merge additional keys, keep the latest version
      }
    }
    
    guard let jsonData = try? JSONEncoder().encode(data) else {
      printToConsole("Failed to Encode \(data)")
      return
    }
    
    if let readyToUpload = await logRotator.write(jsonData, forceRotate: forceUpload) {
      printToConsole("readyToUpload \(readyToUpload.lastPathComponent)")
      await logUploader.upload(readyToUpload)
    }

  }
  
  private func metadataToMap(key: String, _ metadataValue: Logger.MetadataValue) -> [String: String] {
    switch metadataValue {
    case .string(let value):
      return [key: value]
    case .stringConvertible(let value):
      return [key: "\(value)"]
    case .dictionary(let values):
      var retVal = [String: String]()
      values.forEach { (subKey: String, subValue: Logger.MetadataValue) in
        let mappedValue = metadataToMap(key: subKey, subValue)
        retVal.merge(mappedValue, uniquingKeysWith: { $1 }) // Merge additional keys, keep the latest version
      }
      return retVal
    case .array(let values):
      var retVal = [String: String]()
      values.enumerated().forEach { index, subvalue in
        let mappedValue = metadataToMap(key: "\(key)[\(index)]", subvalue)
        retVal.merge(mappedValue, uniquingKeysWith: { $1 }) // Merge additional keys, keep the latest version
      }
      return retVal
    }
  }

  public subscript(metadataKey key: String) -> Logger.Metadata.Value? {
    get {
      return metadata[key]
    }
    set(newValue) {
      metadata[key] = newValue
    }
  }
}
