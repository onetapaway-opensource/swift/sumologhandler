//
//  ConsolePrinter.swift
//

import Foundation

var consolePrintEnabled = true

func printToConsole(_ items: Any..., separator: String = " ") {
  if consolePrintEnabled {
    let message = items.map { "\($0)" }.joined(separator: separator)
    print("🪵 \(message)")
  }
}
