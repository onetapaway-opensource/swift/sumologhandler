//
//  LogFileManager.swift
//

import Foundation

func createLogFile(_ fileUrl: URL) throws {
  if FileManager.default.fileExists(atPath: fileUrl.path) {
    // we already have this file. Let's try to move it with the hope that it will get cleaned up in a minute
    let movedUrl = fileUrl.appendingPathExtension("old")
    do {
      try FileManager.default.moveItem(at: fileUrl, to: movedUrl)
      printToConsole("\(fileUrl.lastPathComponent) renamed to \(movedUrl.lastPathComponent)")
    } catch {
      printToConsole("\(fileUrl.lastPathComponent) could not be renamed to \(movedUrl.lastPathComponent)", error)
      let lastChanceUrl = movedUrl.appendingPathExtension("old")              // we may have found ota-0.log.old so let's try to move it to ota-0.log.old.old with the hope that it will be cleaned up in a minute
      try? FileManager.default.removeItem(at: lastChanceUrl)                  // if ota-0.log.old.old exists, we just need to give up on it. We can't keep trying it forever
      try? FileManager.default.moveItem(at: movedUrl, to: lastChanceUrl)      // try to move the ota-0.log.old file to ota-0.log.old.old with the hope that it will be cleaned up in a minute
      try? FileManager.default.moveItem(at: fileUrl, to: movedUrl)            // try one last time to move ota-0.log to ota-0.log.old
    }
  }
  try "".write(to: fileUrl, atomically: true, encoding: String.Encoding.utf8) // create/overwrite ota-0.log with nothing in it
}

func deleteLogFile(_ fileUrl: URL, failureExtension: String) {
  let fileName = fileUrl.pathComponents.last ?? "invalid log file name"
  do {
    try FileManager.default.removeItem(at: fileUrl)
    printToConsole("\(fileName) deleted successfully")
  } catch {
    printToConsole("LogFileManager.delete failed to delete \(fileName)", error)
    renameLogFile(fileUrl, pathExtension: failureExtension)
  }
}

func renameLogFile(_ fileUrl: URL, pathExtension: String) {
  let fileName = fileUrl.lastPathComponent
  do {
    let failedUrl = fileUrl.appendingPathExtension(pathExtension)
    try FileManager.default.moveItem(at: fileUrl, to: failedUrl)
    printToConsole("\(fileName) renamed to \(failedUrl.lastPathComponent)")
  } catch {
    printToConsole("LogFileManager.rename failed to rename \(fileName)", error)
  }
}
