//
//  LogUploader.swift
//

import Foundation
import Gzip

actor LogUploader {
  
  private let sumoUrl: URL
  private let sourceName: String
  private let sourceHost: String
  private var sourceCategory: String
  private var urlSession = URLSession(configuration: URLSessionConfiguration.default)
  
  var isUploading: Bool = false
  
  init(sumoUrl: URL, sourceName: String, sourceHost: String, sourceCategory: String) {
    self.sumoUrl = sumoUrl
    self.sourceName = sourceName
    self.sourceHost = sourceHost
    self.sourceCategory = sourceCategory
  }
  
  /// gzips and uploads the file at the `fileUrl`
  /// - Parameter fileUrl: the file to be uploaded
  func upload(_ fileUrl: URL) async {
    isUploading = true
    defer { isUploading = false }
    do {
      let logsToUpload = try String(contentsOf: fileUrl, encoding: .utf8)
      if let data = logsToUpload.data(using: .utf8), !data.isEmpty {
        let gz = try data.gzipped(level: .bestCompression)
        
        var request = URLRequest(url: sumoUrl)
        request.setValue("gzip", forHTTPHeaderField: "Content-Encoding")
        request.setValue(sourceName, forHTTPHeaderField: "X-Sumo-Name")
        request.setValue(sourceHost, forHTTPHeaderField: "X-Sumo-Host")
        request.setValue(sourceCategory, forHTTPHeaderField: "X-Sumo-Category")
        request.httpMethod = "POST"
        
        let (_, response) = try await urlSession.upload(for: request, from: gz)
        
        if let httpResponse = response as? HTTPURLResponse, 200..<400 ~= httpResponse.statusCode { // I don't think we care if they redirected it. Just make sure we're below 400
          printToConsole("\(fileUrl.lastPathComponent) uploaded successfully")
          delete(fileUrl, failureExtension: "uploaded")
        } else {
          printToConsole("\(fileUrl.lastPathComponent) upload returned an unexpected response \(response)")
          rename(fileUrl, pathExtension: "err")
        }

      } else {
        printToConsole("\(fileUrl.lastPathComponent) is empty or non-existent. Deleting it")
        delete(fileUrl)
      }
      
    } catch {
      printToConsole("LogUploader.upload caught an error trying to upload \(fileUrl); error = \(error)")
    }
  }
 
  func delete(_ fileUrl: URL, failureExtension: String? = nil) {
    let fileName = fileUrl.pathComponents.last ?? "invalid log file name"
    do {
      try FileManager.default.removeItem(at: fileUrl)
      printToConsole("\(fileName) deleted successfully")
    } catch {
      printToConsole("failed to delete \(fileName)", error)
      if let failureExtension {
        rename(fileUrl, pathExtension: failureExtension)
      }
    }
  }
  
  func rename(_ fileUrl: URL, pathExtension: String) {
    let fileName = fileUrl.lastPathComponent
    do {
      let failedUrl = fileUrl.appendingPathExtension(pathExtension)
      try FileManager.default.moveItem(at: fileUrl, to: failedUrl)
      printToConsole("\(fileName) renamed to \(failedUrl.lastPathComponent)")
    } catch {
      printToConsole("failed to rename \(fileName)", error)
    }
  }
}

