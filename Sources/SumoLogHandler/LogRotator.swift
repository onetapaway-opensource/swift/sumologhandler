//
//  LogRotator.swift
//

import Foundation

actor LogRotator {
  
  private var logIndex: Int = 0
  private var fileHandle: FileHandle? = nil
  private var documentsDirectory: URL? = nil
  
  init() {
    guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
      printToConsole("FAILED TO INITIALIZE LogRotator!!! Could not find documentDirectory")
      return
    }
    documentsDirectory = dir
    let fileName = "ota-\(logIndex).log"
    let fileUrl = dir.appendingPathComponent(fileName)
    do {
      try createLogFile(fileUrl)
      fileHandle = try FileHandle(forWritingTo: fileUrl)
    } catch {
      printToConsole("LogFileManager.create caught an unexpected error", error)
    }
  }
  
  
  /// Writes the jsonData to the current log file.
  /// If the file is ready to be uploaded, the current log file will be rotated, and a URL to the file will be returned
  /// - Parameters:
  ///   - jsonData: the data to write to file. This should be in JSON format.
  ///   - forceRotate: rotates the file regardless of the file's size.
  /// - Returns: a URL to the file that was just rotated. Use this URL to upload the file to Sumo.
  func write(_ jsonData: Data, forceRotate: Bool) -> URL? {
    guard let documentsDirectory, var fileHandle else {
      printToConsole("LogRotator.write cannot continue becase LogRotator failed to initialize properly")
      return nil
    }
    
    do {
      
      fileHandle.seekToEndOfFile()
      fileHandle.write(jsonData)
      fileHandle.write("\n".data(using: .utf8)!)
      
      let filePath = documentsDirectory.appendingPathComponent("ota-\(logIndex).log")
      if forceRotate || checkFileSize(filePath) > 500000 {                              // Sumo recommends the data payload have a size, before compression, of 100KB to 1MB
        printToConsole("rotating \(filePath.lastPathComponent), logIndex: \(logIndex)")
        fileHandle.closeFile()                                                          // close the old file
        logIndex += 1                                                                   // rotate
        let nextPath = documentsDirectory.appendingPathComponent("ota-\(logIndex).log") // new file path with the rotated index
        try createLogFile(nextPath)                                                     // open a new file with nothing in it
        self.fileHandle = try FileHandle(forWritingTo: nextPath)                        // create a new fileHandle that uses the new file
        printToConsole("rotated to \(nextPath.lastPathComponent), logIndex: \(logIndex). returning \(filePath.lastPathComponent)")
        return filePath
      }
    } catch {
      printToConsole("LogRotator.write caught an unexpected; error = \(error)")
    }
    
    return nil // the current file is not ready to be uploaded
  }
  
  /// Returns the size of the file at the `filePath` URL
  /// - Parameter filePath: the URL to check
  /// - Returns: the size of the file in bytes
  private func checkFileSize(_ filePath: URL) -> Int {
    do {
      let value = try filePath.resourceValues(forKeys: [.fileSizeKey])
      return value.fileSize ?? 0
    } catch {
      printToConsole("LogRotator.checkFileSize caught an error checking \(filePath); error = \(error)")
      return 0
    }
  }
  
  func oldFileToCleanUp() -> URL? {
    guard let documentsDirectory, let fileEnumerator = FileManager.default.enumerator(atPath: documentsDirectory.path) else { return nil }
    var highestPriority: String? = nil
    while let element = fileEnumerator.nextObject() as? String {
      guard element.hasSuffix(".err") || element.hasSuffix(".old") else {
        continue
      }
      guard FileManager.default.fileExists(atPath: documentsDirectory.appendingPathComponent(element).path) else {
        continue
      }
      
      if element.hasSuffix(".err") {
        highestPriority = element // this file failed once. try it again before any others
        break
      }
      
      if let currentHighest = highestPriority {
        if currentHighest.hasSuffix(".old.old") {
          // currentHighest is the highest priority (not including .err)
          if element.hasSuffix(".old.old") && currentHighest > element {
            highestPriority = element // lower number is higher priority
          }
        } else if currentHighest.hasSuffix(".log.old") && element.hasSuffix(".old.old") {
          highestPriority = element // .old.old is higher priority than log.old
        } else if currentHighest > element {
          highestPriority = element // lower number is higher priority
        }
      } else {
        // don't have a highest priority yet. Take whatever we found first
        highestPriority = element
      }

    }
    
    if let highestPriority {
      return documentsDirectory.appendingPathComponent(highestPriority)
    }
    return nil
  }
  
}
