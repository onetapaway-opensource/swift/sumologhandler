import XCTest

import SumoLogHandlerTests

var tests = [XCTestCaseEntry]()
tests += SumoLogHandlerTests.allTests()
XCTMain(tests)
