// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "SumoLogHandler",
  platforms: [
    .iOS(.v17)
  ],
  products: [
    .library(
      name: "SumoLogHandler",
      targets: ["SumoLogHandler"]),
  ],
  dependencies: [
    .package(url: "https://github.com/apple/swift-log.git", from: "1.5.3"),
    .package(url: "https://github.com/1024jp/GzipSwift", from: "6.0.1")
  ],
  targets: [
    .target(
      name: "SumoLogHandler",
      dependencies: [
        .product(name: "Logging", package: "swift-log"),
        .product(name: "Gzip", package: "GzipSwift")
      ]),
    .testTarget(
      name: "SumoLogHandlerTests",
      dependencies: ["SumoLogHandler"]),
  ]
)
